﻿using OptimizacionBinaria.Funciones;
using System;

namespace OptimizacionBinaria.Metaheuristicas.EstadoSimple.GLS
{
    public class GLS : Algorithm
    {
        public double lambda = 0.0;
        public int EfoLocalSearch = 10;

        public override void Ejecutar(Knapsack theProblem, Random myRandom)
        {
            EFOs = 0;
            var utilidad = new double[theProblem.TotalItems];
            var penalizacion = new int[theProblem.TotalItems];

            // Inicializacion aleatoria
            var s = new Solution(theProblem, this);
            s.RandomInitialization(myRandom);

            for (var i = 0; i < theProblem.TotalItems; i++)
                penalizacion[i] = 0;

            while (EFOs < MaxEFOs && (s.Fitness != s.MyProblem.OptimalKnown))
            {
                var svariable = this.localsearch(s, penalizacion, myRandom);

                for (var i = 0; i < theProblem.TotalItems; i++)
                    utilidad[i] = svariable.Objects[i] * (theProblem.Weight(i) / (1 + penalizacion[i]));

                var MayorUtilidad = utilidad[0];
                for (var i = 0; i < theProblem.TotalItems; i++)
                {
                    if (utilidad[i] >= MayorUtilidad)
                        MayorUtilidad = utilidad[i];
                }

                for (var i = 0; i < theProblem.TotalItems; i++)
                {
                    if (utilidad[i] == MayorUtilidad)
                        penalizacion[i] += 1;
                }

                if (svariable.Fitness > s.Fitness)
                {
                    s = svariable;
                }
            }
            BestSolution = s;
        }

        public Solution localsearch(Solution s, int[] penalizacion, Random myRandom)
        {
            var EFOls = 0;

            // ciclo de evolución
            while (EFOls < EfoLocalSearch)
            {
                // r es copia de s
                var r = new SolucionGLS(s, penalizacion, lambda);
                //tweak
                r.Tweak(myRandom);

                if (r.Fitness > s.Fitness) //se esta maximizacion
                {
                    s = r;
                }
                EFOls++;
            }

            BestSolution = s;
            return BestSolution;
        }
        public override string ToString()
        {
            return "Busqueda Local Guiada";
        }
    }
}
