﻿using OptimizacionBinaria.Funciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptimizacionBinaria.Metaheuristicas.EstadoSimple.GLS
{
    public class SolucionGLS : Solution
    {
        int[] penalizaciones;
        double varlambda;

        public SolucionGLS(Solution original, int[] penalizacion, double lambda) : base(original)
        {
            varlambda = lambda;
            penalizaciones = new int[penalizacion.Length];
            for (var i = 0; i < penalizacion.Length; i++)
            {
                penalizaciones[i] = penalizacion[i];
            }
        }

        public override void Evaluate()
        {
            base.Evaluate();
            var acumulacion = 0.0;
            for (var i = 0; i < penalizaciones.Length; i++)
            {
                acumulacion += varlambda * (penalizaciones[i] * Objects[i]);
            }
            Fitness -= acumulacion;
        }
    }
}
