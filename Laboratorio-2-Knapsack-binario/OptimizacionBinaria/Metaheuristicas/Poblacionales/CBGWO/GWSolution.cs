﻿using OptimizacionBinaria.Funciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptimizacionBinaria.Metaheuristicas.Poblacionales.CBGWO
{
    class GWSolution : Solution
    {
        public GWSolution(Knapsack theProblem, Algorithm theAlgorithm) : base(theProblem, theAlgorithm)
        {
        }
        public GWSolution(Solution original) : base(original)
        {
        }
        public void UpdatePositionLoser(double A, double C, GWSolution alpha, GWSolution beta, GWSolution delta, 
                                            GWSolution winner, Random myRandom) 
        {
            var X1 = PosLeader(A, C, alpha, winner, this);
            var X2 = PosLeader(A, C, beta, winner, this);
            var X3 = PosLeader(A, C, delta, winner, this);

            Weight = 0;
            for (var i = 0; i < MyProblem.TotalItems; i++)
            {
                var X = ((X1[i] + X2[i] + X3[i]) / 3);
                var sigma = 1 / (1 + Math.Exp(-10 * (X - 0.5)));
                if (sigma >= myRandom.NextDouble()) SelectObject(i);
                else Objects[i] = 0;
            }
        }

        private double[] PosLeader(double A, double C,GWSolution leader, GWSolution winner, GWSolution loser)
        {
            var X = new double[MyProblem.TotalItems];
            var D = new double[MyProblem.TotalItems];
            for (var i = 0; i < MyProblem.TotalItems; i++)
            {
                D[i] = Math.Abs(C * leader.Objects[i] - (winner.Objects[i] - loser.Objects[i]));
            }
            for (var j = 0; j < MyProblem.TotalItems; j++)
            {
                X[j] = Math.Abs(leader.Objects[j] - A * D[j]);
            }
            return X;
        }

        public void LeaderRandomWalk(double R, Random myRandom)
        {
            for (var i = 0; i < MyProblem.TotalItems; i++)
            {
                if (R >= myRandom.NextDouble())
                {
                    if (myRandom.Next(2)==1) SelectObject(i);
                }
            }
            Repare(myRandom);
            Complete(myRandom, new List<int>());
            Evaluate();
        }

        public void UpdatePositionOmega(double A, double C, 
                GWSolution alpha, GWSolution beta, GWSolution delta, Random myRandom)
        {
            var X1 = PosLeaderBGWO2(A, C, alpha, this);
            var X2 = PosLeaderBGWO2(A, C, beta, this);
            var X3 = PosLeaderBGWO2(A, C, delta, this);

            Weight = 0;
            for (var i = 0; i < MyProblem.TotalItems; i++)
            {
                var X = ((X1[i] + X2[i] + X3[i]) / 3);
                var sigma = 1 / (1 + Math.Exp(-10 * (X - 0.5)));
                if (sigma >= myRandom.NextDouble()) SelectObject(i);
                else Objects[i] = 0;
            }
        }
        private double[] PosLeaderBGWO2(double A, double C, GWSolution leader, GWSolution x)
        {
            var X = new double[MyProblem.TotalItems];
            var D = new double[MyProblem.TotalItems];
            for (var i = 0; i < MyProblem.TotalItems; i++)
            {
                D[i] = Math.Abs(C * leader.Objects[i] - x.Objects[i]);
            }
            for (var j = 0; j < MyProblem.TotalItems; j++)
            {
                X[j] = Math.Abs(leader.Objects[j] - A * D[j]);
            }
            return X;
        }

    }
}
