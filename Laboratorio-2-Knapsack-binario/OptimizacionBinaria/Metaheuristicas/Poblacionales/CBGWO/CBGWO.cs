﻿using OptimizacionBinaria.Funciones;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace OptimizacionBinaria.Metaheuristicas.Poblacionales.CBGWO
{
    public class CBGWO : Algorithm
    {
        public int PopulationSize = 100;
        public int MaxIterations = 1000;
        public override void Ejecutar(Knapsack theProblem, Random myRandom)
        {
            EFOs = 0;
            double r1 = myRandom.NextDouble();
            double r2 = myRandom.NextDouble();
            double a = 2.0;
            double A = (2 * a) * r1 - a;
            double C = 2 * r2;

            var Population = new List<GWSolution>();
            var newPopulation = new List<GWSolution>();
            var losers = new List<KeyValuePair<GWSolution, GWSolution>>(); //loser, winner
            for (var i = 0; i < PopulationSize; i++)
            {
                var wolf = new GWSolution(theProblem, this);
                wolf.RandomInitialization(myRandom);
                Population.Add(wolf);
            }

            Population.Sort((x, y) => -1 * x.Fitness.CompareTo(y.Fitness));
            var PosAlpha = new GWSolution(Population[0]);
            var PosBeta = new GWSolution(Population[1]);
            var PosDelta = new GWSolution(Population[2]);

            BestSolution = PosAlpha;
            if (PosAlpha.IsOptimalKnown()) return;

            for (var i = 0; i < MaxIterations; i++)
            {
                double R = 0.9 - 0.9 * (i*1.0 / MaxIterations);
                // Selection of winner and loser
                for (var t = 0; t < PopulationSize/2; t++)
                {
                    var w1 = myRandom.Next(Population.Count);
                    var w2 = myRandom.Next(Population.Count);
                    while (w1 == w2) w2 = myRandom.Next(Population.Count);

                    var wolf1 = Population[w1];
                    var wolf2 = Population[w2];

                    if (wolf1.Fitness > wolf2.Fitness){
                        newPopulation.Add(wolf1);
                        losers.Add(new KeyValuePair<GWSolution, GWSolution>(wolf2,wolf1));
                    }
                    else{
                        newPopulation.Add(wolf2);
                        losers.Add(new KeyValuePair<GWSolution, GWSolution>(wolf1, wolf2));
                    }
                    Population.Remove(wolf1);
                    Population.Remove(wolf2);
                }
                //Loser Update
                for(var l= 0; l<losers.Count; l++)
                {
                    losers[l].Key.UpdatePositionLoser(A, C, PosAlpha, PosBeta, PosDelta, losers[l].Value, myRandom);
                    losers[l].Key.Repare(myRandom);
                    losers[l].Key.Complete(myRandom, new List<int>());
                    losers[l].Key.Evaluate();
                    newPopulation.Add(losers[l].Key);
                    if (EFOs >= MaxEFOs) break;
                }
                Population.AddRange(newPopulation);
                newPopulation.Clear();
                losers.Clear();
                Population.Sort((x, y) => -1 * x.Fitness.CompareTo(y.Fitness));

                //update  parameter
                a = 2 - 2 * (i * 1.0 / MaxIterations);
                A = (2 * a) * r1 - a;
                C = 2 * r2;

                //Leaders enhancement
                for (var leader=0; leader < 3; leader++)
                {
                    var WolfLeader = new GWSolution(Population[leader]);
                    WolfLeader.LeaderRandomWalk(R, myRandom);

                    if (WolfLeader.Fitness > Population[leader].Fitness){
                        Population[leader] = WolfLeader;
                    }
                    if (EFOs >= MaxEFOs) break;
                }
                Population.Sort((x, y) => -1 * x.Fitness.CompareTo(y.Fitness));
                PosAlpha = Population[0];
                PosBeta = Population[1];
                PosDelta = Population[2];

                if (EFOs >= MaxEFOs) break;
                if (PosAlpha.IsOptimalKnown())
                    break;
            }

            BestSolution = PosAlpha;
        }
        public override string ToString()
        {
            return "CBGWO";
        }
    }
}
