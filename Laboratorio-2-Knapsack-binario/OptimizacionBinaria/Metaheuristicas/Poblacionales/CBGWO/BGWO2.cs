﻿using OptimizacionBinaria.Funciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptimizacionBinaria.Metaheuristicas.Poblacionales.CBGWO
{
    class BGWO2 : Algorithm
    {
        public int PopulationSize = 100;
        public int MaxIterations = 1000;
        public override void Ejecutar(Knapsack theProblem, Random myRandom)
        {
            EFOs = 0;
            double r1 = myRandom.NextDouble();
            double r2 = myRandom.NextDouble();
            double a = 2.0;
            double C = 2 * r2;
            double A = (2 * a) * r1 - a;

            var Population = new List<GWSolution>();
            for (var i = 0; i < PopulationSize; i++)
            {
                var wolf = new GWSolution(theProblem, this);
                wolf.RandomInitialization(myRandom);
                Population.Add(wolf);
            }

            Population.Sort((x, y) => -1 * x.Fitness.CompareTo(y.Fitness));
            var PosAlpha = new GWSolution(Population[0]);
            var PosBeta = new GWSolution(Population[1]);
            var PosDelta = new GWSolution(Population[2]);

            BestSolution = PosAlpha;
            if (PosAlpha.IsOptimalKnown()) return;

            for (var t = 0; t < MaxIterations; t++)
            {
                for (var i = 0; i < PopulationSize; i++)
                {
                    Population[i].UpdatePositionOmega(A, C, PosAlpha, PosBeta, PosDelta, myRandom);
                    Population[i].Repare(myRandom);
                    Population[i].Complete(myRandom, new List<int>());
                    Population[i].Evaluate();
                }

                Population.Sort((x, y) => -1 * x.Fitness.CompareTo(y.Fitness));
                PosAlpha = Population[0];
                PosBeta = Population[1];
                PosDelta = Population[2];

                //update  parameter
                a = 2 - 2 * (t * 1.0 / MaxIterations);
                A = (2 * a) * r1 - a;
                C = 2 * r2;

                if (EFOs >= MaxEFOs) break;
                if (PosAlpha.IsOptimalKnown())
                    break;
            }
            
            BestSolution = PosAlpha;
        }

        public override string ToString()
        {
            return "BGWO2";
        }
    }
}
