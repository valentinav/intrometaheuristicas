﻿using System;
using System.Collections.Generic;
using OptimizacionBasica.Funciones;
using OptimizacionBasica.Metaheuristicas.EstadoSimple;
using OptimizacionBasica.Metaheuristicas.EstadoSimple.HC;
using OptimizacionBasica.Metaheuristicas.Poblacionales.AlgoritmoGenetico;

namespace OptimizacionBasica
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = 3;
            var misFunciones = new List<Funcion>
            {
                new Sphere(n),
                new Step(n),
                new Schwefel(n),
                new Rastrigin(n),
                new Griewank(n),
                new Ackley(n)
            };
            var misAlgoritmos = new List<Algoritmo>
            {
                new Genetico1 { PopulationSize = 100, MaxEFOs =1000 }
                /*
                new AscensoColina {pm = 0.5, radio = 10, MaxEFOs = 5000},
                new AscensoColinaMaximaPendiente {pm = 0.5, radio = 10, vecinos = 10, MaxEFOs = 500},
                new AscensoColinaMaximaPendienteConRemplazo {pm = 0.5, radio = 10, vecinos = 10, MaxEFOs = 5000},
                new AscensoColinaConReinicios {ProbabilidadDeMutacion = 0.5, radio = 10, maxLocalIter = 15, MaxEFOs = 5000},
                new BusquedaAleatoria() {MaxEFOs = 5000}*/
            };

            Console.WriteLine("           Ascenso a la Colina (HC)  HC Maxima Pendiente       HC MPendiente Remplazo    HC con Reinicios          Busqueda Aleatoria");
            foreach (var funcion in misFunciones)
            {
                Console.Write($"{funcion,-10}" + " ");

                foreach (var algoritmo in misAlgoritmos)
                {
                    var maxRep = 30;
                    var mediaF = 0.0;
                    for (var rep = 0; rep < maxRep; rep++)
                    {
                        var aleatorio = new Random(rep);
                        algoritmo.Ejecutar(funcion, aleatorio);
                        mediaF += algoritmo.MejorSolucion.fitness;
                    }

                    Console.Write($"{mediaF,-25:0.000000000000000}" + " ");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}