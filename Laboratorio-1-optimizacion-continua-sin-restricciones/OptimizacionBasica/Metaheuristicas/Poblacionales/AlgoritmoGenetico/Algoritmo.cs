﻿using System;
using OptimizacionBasica.Funciones;

namespace OptimizacionBasica.Metaheuristicas.EstadoSimple
{
    public abstract class Algoritmo
    {
        public int MaxEFOs;
        public int EFOs;
        public Solucion MejorSolucion;

        public abstract void Ejecutar(Funcion funcion, Random aleatorio);
    }
}
