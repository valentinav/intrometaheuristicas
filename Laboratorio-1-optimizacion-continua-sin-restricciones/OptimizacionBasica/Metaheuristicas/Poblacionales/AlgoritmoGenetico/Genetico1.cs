﻿using OptimizacionBasica.Funciones;
using OptimizacionBasica.Metaheuristicas.EstadoSimple;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OptimizacionBasica.Metaheuristicas.Poblacionales.AlgoritmoGenetico
{
    class Genetico1 : Algoritmo
    {
        public int PopulationSize = 100;
        public int MaxGenerations = 1000;
        public double radio = 1;
        public double MutationProbability = 0.5;
        public override void Ejecutar(Funcion funcion, Random aleatorio)
        {
            EFOs = 0;
            var Population = new List<Solucion>();
            for (var i=0; i < PopulationSize; i++)
            {
                var s = new Solucion(funcion,this);
                s.InicializarAleatorio(aleatorio);
                Population.Add(s);
            }

            for (var g = 1; g < MaxGenerations; g++)
            {
                var OffSprints = new List<Solucion>();
                for (var offSpr = 0; offSpr < PopulationSize/2; offSpr++)
                {
                    //selección de padre por torneo
                    var p1 = aleatorio.Next(PopulationSize);
                    var p2 = aleatorio.Next(PopulationSize);
                    while (p1 == p2)
                    {
                        p2 = aleatorio.Next(PopulationSize);
                    }
                    Solucion Padre = Population[p2];
                    if (Population[p1].fitness < Population[p2].fitness)
                        Padre = Population[p1];

                    //Selección de madre por torneo
                    p1 = aleatorio.Next(PopulationSize);
                    p2 = aleatorio.Next(PopulationSize);
                    while (p1 == p2)
                    {
                        p2 = aleatorio.Next(PopulationSize);
                    }
                    Solucion Madre = Population[p2];
                    if (Population[p1].fitness < Population[p2].fitness) //Minimizando
                        Madre = Population[p1];

                    var h1 = new Solucion(funcion, this);
                    var h2 = new Solucion(funcion, this);

                    var puntoCruce = aleatorio.Next(funcion.TotalDimensiones-1);

                    h1.Cruce(Padre, Madre, puntoCruce);
                    h2.Cruce(Madre, Padre, puntoCruce);

                    h1.OneBitMutation(aleatorio,radio);
                    h1.Evaluar();
                    OffSprints.Add(h1);

                    h2.OneBitMutation(aleatorio, radio);
                    h2.Evaluar();
                    OffSprints.Add(h2);
                    
                }
            }
        }
    }
}
