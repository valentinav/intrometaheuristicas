﻿using System;
using OptimizacionBasica.Funciones;
using OptimizacionBasica.Metaheuristicas.EstadoSimple;

namespace OptimizacionBasica.Metaheuristicas
{
    public class Solucion
    {
        private double[] dimensiones;
        public double fitness;
        public Funcion miFuncion;
        public EstadoSimple.Algoritmo MiAlgoritmo;

        public Solucion(Funcion laFuncion, Algoritmo miAlgo)
        {
            miFuncion = laFuncion;
            MiAlgoritmo = miAlgo;
            dimensiones = new double[miFuncion.TotalDimensiones];
        }

        public Solucion(Solucion original)
        {
            miFuncion = original.miFuncion;
            MiAlgoritmo = original.MiAlgoritmo;
            fitness = original.fitness;
            dimensiones = new double[miFuncion.TotalDimensiones];
            for(var i = 0; i < miFuncion.TotalDimensiones; i++)
                dimensiones[i] = original.dimensiones[i];
        }

        public void InicializarAleatorio(Random aleatorio)
        {
            for (var i = 0; i < miFuncion.TotalDimensiones; i++)
                dimensiones[i] = miFuncion.LimiteInferior +
                       (miFuncion.LimiteSuperior - miFuncion.LimiteInferior) * aleatorio.NextDouble();
            Evaluar();
        }

        public void Tweak(Random aleatorio, double pm, double radio)
        {
            for (var i = 0; i < miFuncion.TotalDimensiones; i++)
            {
                if (aleatorio.NextDouble() < pm)
                {
                    dimensiones[i] = dimensiones[i] + (-radio + 2 * radio * aleatorio.NextDouble());
                    if (dimensiones[i] < miFuncion.LimiteInferior) dimensiones[i] = miFuncion.LimiteInferior;
                    if (dimensiones[i] > miFuncion.LimiteSuperior) dimensiones[i] = miFuncion.LimiteSuperior;
                }
            }
            Evaluar();
        }

        public void OneBitMutation(Random aleatorio, double radio)
        {
            var i = aleatorio.Next(miFuncion.TotalDimensiones);             
            dimensiones[i] = dimensiones[i] + (-radio + 2 * radio * aleatorio.NextDouble());
            if (dimensiones[i] < miFuncion.LimiteInferior) dimensiones[i] = miFuncion.LimiteInferior;
            if (dimensiones[i] > miFuncion.LimiteSuperior) dimensiones[i] = miFuncion.LimiteSuperior;
        }

        public void Cruce( Solucion P1, Solucion P2, int puntoCruce)
        {
            for (var i = 0; i < miFuncion.TotalDimensiones; i++)
            {
                if (i <= puntoCruce)
                    this.dimensiones[i] = P1.dimensiones[i];
                else
                    this.dimensiones[i] = P2.dimensiones[i];
            }
        }

        public void Evaluar()
        {
            fitness = miFuncion.Evaluar(dimensiones);
            MiAlgoritmo.EFOs++;
        }

        public override string ToString()
        {
            var result = "";
            for (var i = 0; i < miFuncion.TotalDimensiones; i++)
                result = result + (dimensiones[i] + " ");
            result = result + "   f(s) = " + fitness;
            return result;
        }
    }
}
