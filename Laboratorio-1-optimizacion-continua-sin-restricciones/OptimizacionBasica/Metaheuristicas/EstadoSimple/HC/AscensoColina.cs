﻿using System;
using System.Diagnostics;
using OptimizacionBasica.Funciones;

namespace OptimizacionBasica.Metaheuristicas.EstadoSimple.HC
{
    public class AscensoColina: Algoritmo
    {
        public double pm = 0.5;
        public double radio = 10;

        public override void Ejecutar(Funcion funcion, Random aleatorio)
        {
            EFOs = 0;

            // Inicializacion aleatoria
            var s = new Solucion(funcion, this);
            s.InicializarAleatorio(aleatorio);
            //Debug.WriteLine(EFOs + " " + s.fitness);

            // ciclo de evolución
            while (EFOs < MaxEFOs)
            {
                // r es copia de s
                var r = new Solucion(s);
                //tweak
                r.Tweak(aleatorio, pm, radio);

                if (r.fitness < s.fitness) //se esta minimizando
                {
                    s = r;
                }
                //Debug.WriteLine(EFOs + " " + s.fitness);
            }

            MejorSolucion = s;
        }

        public override string ToString()
        {
            return "Ascenso a la Colina";
        }
    }
}
