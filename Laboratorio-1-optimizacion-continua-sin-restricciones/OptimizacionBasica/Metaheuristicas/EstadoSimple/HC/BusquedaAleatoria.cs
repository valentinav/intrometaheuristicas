﻿using System;
using System.Diagnostics;
using OptimizacionBasica.Funciones;

namespace OptimizacionBasica.Metaheuristicas.EstadoSimple.HC
{
    public class BusquedaAleatoria:Algoritmo
    {
        public override void Ejecutar(Funcion funcion, Random aleatorio)
        {
            EFOs = 0;
            // Inicializacion aleatoria
            MejorSolucion = new Solucion(funcion, this);
            MejorSolucion.InicializarAleatorio(aleatorio);
            Debug.WriteLine(EFOs + " " + MejorSolucion.fitness);

            // ciclo de evolución
            while (EFOs < MaxEFOs)
            {
                // r es otra solucion aleatoria
                var r = new Solucion(funcion, this);
                r.InicializarAleatorio(aleatorio);

                if (r.fitness < MejorSolucion.fitness) //se esta minimizando
                {
                    MejorSolucion = r;
                }
                Debug.WriteLine(EFOs + " " + MejorSolucion.fitness);
            }
        }

        public override string ToString()
        {
            return "Búsqueda Aleatoria";
        }
    }
}
