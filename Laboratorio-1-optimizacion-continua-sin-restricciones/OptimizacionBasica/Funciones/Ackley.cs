﻿using System;

namespace OptimizacionBasica.Funciones
{
    /// <summary>
    /// Funcion Ackely con minimo 2 dimensiones
    /// </summary>
    public class Ackley : Funcion
    {
        public Ackley(int n)
        {
            if (n < 2) throw new Exception("Ackley debe tener minimo 2 dimensiones");

            TotalDimensiones = n;
            LimiteInferior = -32;
            LimiteSuperior = 32;
        }

        public override double Evaluar(double[] valoresSolucion)
        {
            const double a = 20;
            const double b = 0.2;
            const double c = 2 * Math.PI;
            double dx = 0;
            double dy = 0;

            foreach (var value in valoresSolucion)
            {
                dx += Math.Pow(value, 2);
                dy += Math.Cos(c * value);
            }

            var result = -a * Math.Exp(-b * Math.Sqrt(dx / TotalDimensiones)) -
                              Math.Exp(dy / TotalDimensiones) +
                              a + Math.Exp(1);
            return result;
        }

        public override string ToString()
        {
            return "Ackley";
        }
    }
}
