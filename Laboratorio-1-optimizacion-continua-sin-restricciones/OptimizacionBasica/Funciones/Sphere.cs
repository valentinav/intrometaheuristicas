﻿using System;

namespace OptimizacionBasica.Funciones
{
    public class Sphere : Funcion
    {
        public Sphere(int n)
        {
            TotalDimensiones = n;
            LimiteInferior = -100;
            LimiteSuperior = 100;
        }

        public override double Evaluar(double[] valoresSolucion)
        {
            var suma = 0.0;
            for (var i = 0; i < TotalDimensiones; i++)
            {
                suma = suma + Math.Pow(valoresSolucion[i], 2);
            }

            return suma;
        }

        public override string ToString()
        {
            return "Sphere";
        }
    }
}
