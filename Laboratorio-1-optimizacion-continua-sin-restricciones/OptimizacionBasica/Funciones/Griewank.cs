﻿using System;
using System.Linq;

namespace OptimizacionBasica.Funciones
{
    public class Griewank : Funcion
    {
        public Griewank(int n)
        {
            TotalDimensiones = n;
            LimiteInferior = -600;
            LimiteSuperior = 600;
        }

        public override double Evaluar(double[] valoresSolucion)
        {
            const double a = 4000;
            double b = 1;

            var sum = valoresSolucion.Sum(x => Math.Pow(x, 2));

            for (var i = 0; i < TotalDimensiones; i++)
            {
                b *= Math.Cos(valoresSolucion[i] / (Math.Sqrt(i + 1)));
            }

            double result = (sum / a) - b + 1;
            return result;
        }

        public override string ToString()
        {
            return "Griewank";
        }
    }
}
