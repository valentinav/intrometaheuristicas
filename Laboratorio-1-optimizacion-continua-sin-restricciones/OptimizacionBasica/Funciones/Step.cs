﻿using System;

namespace OptimizacionBasica.Funciones
{
    public class Step : Funcion
    {
        public Step(int n)
        {
            TotalDimensiones = n;
            LimiteInferior = -100;
            LimiteSuperior = 100;
        }

        public override double Evaluar(double[] valoresSolucion)
        {
            var suma = 0.0;
            for (var i = 0; i < TotalDimensiones; i++)
            {
                suma = suma + Math.Pow(Math.Truncate(valoresSolucion[i] + 0.5), 2);
            }

            return suma;
        }

        public override string ToString()
        {
            return "Step";
        }
    }
}
