﻿namespace OptimizacionBasica.Funciones
{
    public abstract class Funcion
    {
        public int TotalDimensiones;
        public double LimiteInferior;
        public double LimiteSuperior;

        public abstract double Evaluar(double[] valoresSolucion);
    }
}
