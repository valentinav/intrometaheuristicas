﻿using System;

namespace OptimizacionBasica.Funciones
{
    public class Schwefel : Funcion
    {
        public Schwefel(int n)
        {
            TotalDimensiones = n;
            LimiteInferior = -100;
            LimiteSuperior = 100;
        }

        public override double Evaluar(double[] valoresSolucion)
        {
            var suma = 0.0;
            for (var i = 0; i < TotalDimensiones; i++)
            {
                var sum = 0d;
                for (var j = 0; j <= i; j++)
                    sum += valoresSolucion[j];
                suma += Math.Pow(sum, 2);
            }

            return suma;
        }

        public override string ToString()
        {
            return "Schwefel";
        }
    }
}