﻿using System;

namespace OptimizacionBasica.Funciones
{
    /// <summary>
    /// Funcion Ackely con minimo 2 dimensiones
    /// </summary>
    public class Rastrigin : Funcion
    {
        public Rastrigin(int n)
        {
            if (n < 1) throw new Exception("Rastrigin debe tener minimo 1 dimension");

            TotalDimensiones = n;
            LimiteInferior = -5.12;
            LimiteSuperior = 5.12;
        }

        public override double Evaluar(double[] valoresSolucion)
        {
            double suma = 0.0;
            for (var i = 0; i < TotalDimensiones; i++)
                suma+= Math.Pow(valoresSolucion[i], 2) - 10 * Math.Cos(2 * Math.PI * valoresSolucion[i]) + 10;

            return suma;
        }

        public override string ToString()
        {
            return "Rastrigin";
        }
    }
}
